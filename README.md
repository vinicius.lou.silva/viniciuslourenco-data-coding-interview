# Data Engineer Coding Exercise

To execute the exercise properly, you will need to:

1) Go to the challenge1 folder and run a `docker compose up -d` command to set up the PostgreSQL database;
2) Run a `pip install -r requirements` in the same folder to install the requirements;
3) After that you can just simple run `python ingest_data.py` to to the first part of the exercise and load the data to the database;
4) After that, if you have your dbt profile configured to connect to the database used in this exercise, with the same credentials, them you need to go to challenge1/dbt/nyxa and run a `dbt run` command to generate the flights_summary, i.e, the second part of the exercise. 

An alternative is just go to challenge1 folder, open a CMD and run `start run_exercise.bat`! 
## Credits

The NYC Flights 2013 dataset published by [aephidayatuloh](https://www.kaggle.com/aephidayatuloh) was extracted from [Kaggle](https://www.kaggle.com/aephidayatuloh/nyc-flights-2013).
