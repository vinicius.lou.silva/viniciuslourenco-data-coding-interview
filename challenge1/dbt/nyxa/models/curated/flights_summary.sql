select
	airline,
    origin.flight,
    flight_date,
    tailnum,
    sched_dep_time,
    actual_dep_time,
    dep_delay,
    sched_arr_time,
    actual_arr_time,
    arr_delay,
    origin_faa,
    origin_name,
    origin_latitude,
    origin_longitude,
    origin_altitude,
    origin_temp,
    origin_dewp,
    origin_humid,
    origin_precip,
    origin_pressure,
    origin_visib,
    dest_faa,
    dest_name,
    dest_latitude,
    dest_longitude,
    dest_altitude,
    air_time,
    distance,
    airplane_model,
    airplane_seats
from
	(
	select
		t2.name as airline,
		concat_ws('-',t2.carrier,flight) as flight,
		to_date(concat_ws('-',t1.year,t1.month,t1.day),'YYYY-MM-DD') as flight_date,
		concat(t1.year,t1.month,t1.day,t1.hour,t1.minute) as time_key,
		t1.tailnum,
		sched_dep_time,
		actual_dep_time,
		dep_delay,
		sched_arr_time,
		actual_arr_time,
		arr_delay,
		air_time,
		distance,
		concat_ws(' ',t5.manufacturer,t5.model) as airplane_model,
		t5.seats as airplane_seats,
		t1.origin as origin_faa,
		t3.name as origin_name,
		t3.latitude as origin_latitude,
		t3.longitude as origin_longitude,
		t3.altitude as origin_altitude,
		t4.temp as origin_temp,
		t4.dewp as origin_dewp,
		t4.humid as origin_humid,
		t4.precip as origin_precip,
		t4.pressure as origin_pressure,
		t4.visib as origin_visib
	from {{ source('dw_flights', 'flights') }} as t1
	inner join {{ source('dw_flights', 'airlines') }} as t2
on t1.carrier = t2.carrier
	inner join {{ source('dw_flights', 'airports') }} as t3
on t1.origin = t3.faa
	inner join {{ source('dw_flights', 'weather') }} as t4
on t1.origin = t4.origin
and t1.time_hour = t4.time_hour
	inner join {{ source('dw_flights', 'planes') }} as t5
on t1.tailnum = t5.tailnum) as origin
inner join (
	select
		concat_ws('-',t2.carrier,flight) as flight,
		concat(t1.year,t1.month,t1.day,t1.hour,t1.minute) as time_key,
		t1.dest as dest_faa,
		t3.name as dest_name,
		t3.latitude as dest_latitude,
		t3.longitude as dest_longitude,
		t3.altitude as dest_altitude
	from {{ source('dw_flights', 'flights') }} as t1
	inner join {{ source('dw_flights', 'airlines') }} as t2
on t1.carrier = t2.carrier
	inner join {{ source('dw_flights', 'airports') }} as t3
on t1.dest = t3.faa) as dest
on origin.flight = dest.flight
and origin.time_key = dest.time_key