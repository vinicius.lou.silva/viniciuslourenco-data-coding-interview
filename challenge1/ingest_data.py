import time
from os import listdir
from os.path import isfile, join
import pandas as pd
import psycopg2
import numpy as np
from io import StringIO
import multiprocessing as mp


def copy_from_stringio(conn, df, table):
    """
    Here we are going save the dataframe in memory 
    and use copy_from() to copy it to the table
    """
    # save dataframe to an in memory buffer
    conn.autocommit = True
    cursor = conn.cursor()
    
    sql = f'''SELECT * FROM {table} limit 1'''
    
    cursor.execute(sql)

    column_names = [desc[0] for desc in cursor.description]

    df = df[column_names]

    buffer = StringIO()
    df.to_csv(buffer,  header=False, index=False)
    buffer.seek(0)

    try:
        t_start = time.time()
        cursor.copy_from(buffer, table, sep=",", columns=column_names, null="")
    except (Exception, psycopg2.DatabaseError) as error:
        print("Error: %s" % error)
        conn.rollback()
        cursor.close()
        return 1

    t_end = time.time()
    print(f'Table {table}, copy_from() done, took {t_end - t_start:.3f} seconds')
    cursor.close()


def ins_into_db(csv_name):
    # user = params.user
    # password = params.password
    # host = params.host
    # port = params.port
    # db = params.db
    # table_name = params.table_name
    # url = params.url

    user = 'postgres'
    password = 'Password1234**'
    db = 'dw_flights'
    port = '5432'
    host = 'localhost'
    table_name = csv_name.split("_")[1].split(".")[0]

    engine = psycopg2.connect(f'postgresql://{user}:{password}@{host}:{port}/{db}')

    df = pd.read_csv(
        csv_name,
        na_values = ['\\N', 'NA'])
    
    dict = {'lat': 'latitude',
            'lon': 'longitude',
            'alt': 'altitude',
            'tz': 'timezone',
            'tzone' : 'timezone_name',
            'dep_time': 'actual_dep_time',
            'arr_time': 'actual_arr_time'}
   
    df.rename(columns=dict,inplace=True)

    if table_name == 'airports': 
        tz_tz_name = df.drop_duplicates('timezone')[['timezone', 'timezone_name']].to_dict('split')['data']
        tz_dict = {x[0]:x[1] for x in tz_tz_name}
        df['timezone_name'] = df['timezone_name'].fillna(df['timezone'].map(tz_dict))        
    elif table_name == 'planes':
        df['year']= df['year'].astype('Int64')
    elif table_name == 'weather': 
         df['year'] = pd.to_datetime(df["time_hour"]).dt.year
         df['month'] = pd.to_datetime(df["time_hour"]).dt.month
         df['day'] =  pd.to_datetime(df["time_hour"]).dt.day
         df['hour'] =  pd.to_datetime(df["time_hour"]).dt.hour
          
    copy_from_stringio(engine,df,table_name)


def run_parallel_sim():
        # Do the parallel insertion into DB
        data_path = 'dataset/'
        pool = mp.Pool(mp.cpu_count())
        pool.map(ins_into_db, [data_path + f for f in listdir(data_path) if isfile(join(data_path, f))])
        pool.close()
        pool.join()


if __name__ == '__main__':
    run_parallel_sim()





